#include "fmt/format.h"

#include "dumbocpp.h"


int main()
{
    if (!Dumbo::run())
    {
        fmt::print("DumboCpp failed...\n");
        return  1;
    }

    return 0;
}
