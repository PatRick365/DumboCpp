#include "dumbocpp.h"

#include <exception>
#include <sstream>
#include <string>
#include <vector>
#include <iomanip>

#include "replxx.hxx"
#include "fmt/format.h"
#include "fmt/color.h"

#include "utility.hpp"


using Replxx = replxx::Replxx;

namespace Dumbo
{

static std::vector<DumboFnBase *> &getDumboList()
{
    static std::vector<DumboFnBase *> dumbos;
    return dumbos;
}

struct DumboFnData
{
    std::string name;
    std::vector<std::string> args;
};

static DumboFnData parseInput(const std::string &input)
{
    std::istringstream iss(input);

    DumboFnData fnData;

    // first string is the function name
    if (!(iss >> std::quoted(fnData.name)))
    {
        throw std::logic_error(fmt::format("parsing of \"{}\" failed..", input));
    }

    // the rest are the arguments
    std::string arg;
    while (iss >> std::quoted(arg))
    {
        fnData.args.push_back(arg);
    }
    return fnData;
}

bool run()
{
    replxx::Replxx replxx;
    const std::vector<DumboFnBase *> &dumbos = getDumboList();

    // init the repl
    replxx.install_window_change_handler();

    // the path to the history file
    std::string historyFile("dumbo_history.txt");

    // load the history file if it exists
    replxx.history_load(historyFile);

    // set the max history size
    replxx.set_max_history_size(128);

    // set the max number of hint rows to show
    replxx.set_max_hint_rows(5);

    replxx.set_completion_callback([&dumbos](const std::string &context, int &contextLen)
    {
        Replxx::completions_t completions;
        int utf8ContextLen = context_len(context.c_str());
        int prefixLen = static_cast<int>(context.length()) - utf8ContextLen;
        if ((prefixLen > 0) && (context[prefixLen - 1] == '\\'))
        {
            --prefixLen;
            ++utf8ContextLen;
        }
        contextLen = utf8str_codepoint_len(context.c_str() + prefixLen, utf8ContextLen);

        std::string prefix = context.substr(prefixLen);

        //fmt::print("context ({}) prefix({}) prefixLen({})\n", context, prefix, prefixLen);

        for (DumboFnBase *dumboFn : dumbos)
        {
            if (dumboFn->dumboFnData.name.compare(0, prefix.size(), prefix) == 0)
            {
                completions.emplace_back(dumboFn->dumboFnData.name.c_str(), Replxx::Color::DEFAULT);
            }
        }

        return completions;
    });

    replxx.set_highlighter_callback([&dumbos](const std::string &context, Replxx::colors_t &colors) -> void
    {
        for (DumboFnBase *dumboFn : dumbos)
        {
            size_t pos = context.find(dumboFn->dumboFnData.name);
            if (pos != std::string::npos)
            {
                // color functions green. everthing after are arguments and are colored blue.
                for (size_t i = 0; i < context.length(); ++i)
                {
                    colors.at(i) = (i < dumboFn->dumboFnData.name.length() + pos) ? Replxx::Color::GREEN : Replxx::Color::BLUE;
                }
                return;
            }
        }
    });

    replxx.set_hint_callback([&dumbos](const std::string &context, int &contextLen, Replxx::Color &color) -> Replxx::hints_t
    {
        Replxx::hints_t hints;

        // only show hint if prefix is at least 'n' chars long
        // or if prefix begins with a specific character
        int utf8ContextLen = context_len(context.c_str());
        int prefixLen = static_cast<int>(context.length()) - utf8ContextLen;
        contextLen = utf8str_codepoint_len(context.c_str() + prefixLen, utf8ContextLen);
        std::string prefix(context.substr(prefixLen));

        if (prefix.size() >= 2)
        {
            for (const DumboFnBase *dumbo : dumbos)
            {
                std::string e = dumbo->dumboFnData.name;

                if (e.compare(0, prefix.size(), prefix) == 0)
                {
                    if (dumbo->dumboFnData.argCount == 0)
                    {
                        hints.push_back(e);
                    }
                    else
                    {
                        hints.push_back(e + dumbo->dumboFnData.signature);
                    }
                }
            }
        }

        // set hint color to yellow if single match found
        if (hints.size() == 1)
        {
            color = Replxx::Color::YELLOW;
        }

        return hints;
    });

    // other api calls
    replxx.set_word_break_characters(" \t.,-%!;:=*~^'\"/?<>|[](){}");
    replxx.set_completion_count_cutoff(128);
    replxx.set_double_tab_completion(false);
    replxx.set_complete_on_empty(true);
    replxx.set_beep_on_ambiguous_completion(false);
    replxx.set_no_color(false);

    // key bindings
    replxx.bind_key_internal(Replxx::KEY::BACKSPACE, "delete_character_left_of_cursor");
    replxx.bind_key_internal(Replxx::KEY::DELETE, "delete_character_under_cursor");
    replxx.bind_key_internal(Replxx::KEY::LEFT, "move_cursor_left");
    replxx.bind_key_internal(Replxx::KEY::RIGHT, "move_cursor_right");
    replxx.bind_key_internal(Replxx::KEY::UP, "history_previous");
    replxx.bind_key_internal(Replxx::KEY::DOWN, "history_next");
    replxx.bind_key_internal(Replxx::KEY::PAGE_UP, "history_first");
    replxx.bind_key_internal(Replxx::KEY::PAGE_DOWN, "history_last");
    replxx.bind_key_internal(Replxx::KEY::HOME, "move_cursor_to_begining_of_line");
    replxx.bind_key_internal(Replxx::KEY::END, "move_cursor_to_end_of_line");
    replxx.bind_key_internal(Replxx::KEY::TAB, "complete_line");
    replxx.bind_key_internal(Replxx::KEY::control(Replxx::KEY::LEFT), "move_cursor_one_word_left");
    replxx.bind_key_internal(Replxx::KEY::control(Replxx::KEY::RIGHT), "move_cursor_one_word_right");
    replxx.bind_key_internal(Replxx::KEY::control(Replxx::KEY::UP), "hint_previous");
    replxx.bind_key_internal(Replxx::KEY::control(Replxx::KEY::DOWN), "hint_next");
    replxx.bind_key_internal(Replxx::KEY::control(Replxx::KEY::ENTER), "commit_line");
    //m_replxx.bind_key_internal(Replxx::KEY::control(Replxx::KEY::BACKSPACE), "kill_to_whitespace_on_left");
    replxx.bind_key_internal(Replxx::KEY::control('R'), "history_incremental_search");
    replxx.bind_key_internal(Replxx::KEY::control('L'), "clear_screen");
    replxx.bind_key_internal(Replxx::KEY::control('D'), "send_eof");
    replxx.bind_key_internal(Replxx::KEY::control('C'), "abort_line");
#ifndef _WIN32
    replxx.bind_key_internal(Replxx::KEY::control('V'), "verbatim_insert");
    replxx.bind_key_internal(Replxx::KEY::control('Z'), "suspend");
#endif
    //m_replxx.bind_key_internal('a', "insert_character");
    replxx.bind_key_internal(Replxx::KEY::INSERT, "toggle_overwrite_mode");

    fmt::print("Welcome to DumboCpp! You can exit by pressing Ctrl+D at any time...\n"
               "Press 'tab' to view autocompletions\n\n");

    std::string prompt("\x1b[1;32mDumboCpp\x1b[0m> ");

    // main repl loop
    for (;;)
    {
        // display the prompt and retrieve input from the user
        const char *cinput = nullptr;

        do
        {
            cinput = replxx.input(prompt);
        }
        while ((cinput == nullptr) && (errno == EAGAIN));

        if (cinput == nullptr)
        {
            break;
        }

        std::string input(cinput);

        if (input.empty())
            continue;

        DumboFnData parsedInput = parseInput(input);

        replxx.history_add(input);

        auto it = std::find_if(dumbos.cbegin(), dumbos.cend(), [&parsedInput](DumboFnBase *dumboFn)
        {
            return dumboFn->dumboFnData.name == parsedInput.name;
        });

        if (it == dumbos.cend())
        {
            fmt::print(fg(fmt::color::red), "error: function \"{}\" not found.", parsedInput.name);
            fmt::print("\n");   // prevent color bleeding into repl prompt
            continue;
        }

        DumboFnBase *dumboFn = *it;

        if (parsedInput.args.size() < dumboFn->dumboFnData.argCount)
        {
            fmt::print(fg(fmt::color::red), "error: not enough arguments({} provided, {} required). signature is {}{}",
                       parsedInput.args.size(), dumboFn->dumboFnData.argCount, dumboFn->dumboFnData.name, dumboFn->dumboFnData.signature);
            fmt::print("\n");   // prevent color bleeding into repl prompt
            continue;
        }
        if (parsedInput.args.size() > dumboFn->dumboFnData.argCount)
        {
            fmt::print(fg(fmt::color::red), "error: too many arguments({} provided, {} required). signature is {}{}",
                       parsedInput.args.size(), dumboFn->dumboFnData.argCount, dumboFn->dumboFnData.name, dumboFn->dumboFnData.signature);
            fmt::print("\n");   // prevent color bleeding into repl prompt
            continue;
        }

        try // catch exceptions from inside a dumbo
        {
            TimeStamp timeStamp;
            if (dumboFn->invokeWithStrings(parsedInput.args))
            {
                fmt::print(fg(fmt::color::gray), "executing {} took {}", dumboFn->dumboFnData.name, timeStamp.elapsedString());
                fmt::print("\n");   // prevent color bleeding into repl prompt
            }
            else
            {
                fmt::print(fg(fmt::color::red), "error while trying to invoke the function: signature is {}{}", dumboFn->dumboFnData.name, dumboFn->dumboFnData.signature);
                fmt::print("\n");   // prevent color bleeding into repl prompt
            }
        }
        catch (const std::exception &exception)
        {
            fmt::print(fg(fmt::color::red), "{} threw an exception:\n\t{}", dumboFn->dumboFnData.name, exception.what());
            fmt::print("\n");   // prevent color bleeding into repl prompt
        }
        catch (...)
        {
            fmt::print(fg(fmt::color::red), "{} threw an unknown exception...", dumboFn->dumboFnData.name);
            fmt::print("\n");   // prevent color bleeding into repl prompt
        }
    }

    fmt::print("\n");

    // save the history
    replxx.history_sync(historyFile);

    return true;
}

DumboFnBase::DumboFnBase(const std::string &name, size_t argCount, const std::string &signature, const char *file, int line)
    : dumboFnData { name, argCount, signature, file, line }
{
    getDumboList().push_back(this);
}

}
