#pragma once

#include <cstring>
#include <chrono>
#include <string>

#include "fmt/format.h"

using namespace std::chrono_literals;


int utf8str_codepoint_len(char const *s, int utf8len)
{
    int codepointLen = 0;
    unsigned char m4 = 128 + 64 + 32 + 16;
    unsigned char m3 = 128 + 64 + 32;
    unsigned char m2 = 128 + 64;
    for (int i = 0; i < utf8len; ++ i, ++ codepointLen)
    {
        char c = s[i];
        if ((c & m4) == m4)
        {
            i += 3;
        }
        else if ((c & m3) == m3)
        {
            i += 2;
        }
        else if ((c & m2) == m2)
        {
            i += 1;
        }
    }
    return codepointLen;
}

int context_len(char const *prefix)
{
    char const wb[] = " \t\n\r";
    int i = int(strlen(prefix)) - 1;
    int cl = 0;
    while ( i >= 0 )
    {
        if (strchr(wb, prefix[i]))
        {
            break;
        }
        ++ cl;
        -- i;
    }
    return cl;
}

class TimeStamp
{
public:
    TimeStamp()
    {
        start();
    }

    void start()
    {
        m_timepoint = std::chrono::high_resolution_clock::now();
    }

    template<typename T = std::chrono::microseconds>
    [[nodiscard]] T elapsed() const
    {
        auto t2 = std::chrono::high_resolution_clock::now();
        return std::chrono::duration_cast<T>(t2 - m_timepoint);
    }

    [[nodiscard]] std::string elapsedString() const
    {
        auto durationNanoseconds = elapsed<std::chrono::nanoseconds>();

        if (std::chrono::abs(durationNanoseconds) < 1us)
        {
            return fmt::format("{}ns", durationNanoseconds.count());
        }
        else if (std::chrono::abs(durationNanoseconds) < 1ms)
        {
            auto musec = std::chrono::duration_cast<std::chrono::microseconds>(durationNanoseconds);
            return fmt::format("{}µs", musec.count());
        }
        else if (std::chrono::abs(durationNanoseconds) < 10s)
        {
            auto msec = std::chrono::duration_cast<std::chrono::milliseconds>(durationNanoseconds);
            durationNanoseconds -= msec;
            auto musec = std::chrono::duration_cast<std::chrono::microseconds>(durationNanoseconds);
            return fmt::format("{}{}ms", msec.count(), (((musec.count() > 0) ? fmt::format(".{}", musec.count()) : "")));
        }
        else if (std::chrono::abs(durationNanoseconds) < 1min)
        {
            auto sec = std::chrono::duration_cast<std::chrono::seconds>(durationNanoseconds);
            durationNanoseconds -= sec;
            auto msec = std::chrono::duration_cast<std::chrono::milliseconds>(durationNanoseconds);
            durationNanoseconds -= msec;
            return fmt::format("{}.{}s", sec.count(), msec.count());
        }

        auto min = std::chrono::duration_cast<std::chrono::minutes>(durationNanoseconds);
        durationNanoseconds -= min;
        auto sec = std::chrono::duration_cast<std::chrono::seconds>(durationNanoseconds);
        durationNanoseconds -= sec;
        auto msec = std::chrono::duration_cast<std::chrono::milliseconds>(durationNanoseconds);
        durationNanoseconds -= msec;
        return fmt::format("{}min, {}.{}s", min.count(), sec.count(), msec.count());    }

private:
    std::chrono::time_point<std::chrono::high_resolution_clock> m_timepoint;
};

