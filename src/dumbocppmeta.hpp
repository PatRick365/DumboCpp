#pragma once

#include <tuple>
#include <string>
#include <vector>
#include <sstream>
#include <optional>
#include <type_traits>


namespace  DumboMeta
{

template<typename T>
std::optional<T> fromString(const std::string &str)
{
    if constexpr (std::is_same<T, std::string>())
        return str;

    T val;
    std::istringstream ss(str);
    ss >> val;
    if (ss && (ss >> std::ws, ss.rdbuf()->in_avail() <= 0))
        return val;
    return std::nullopt;
}

namespace Detail
{

// Convert array into a tuple
template<typename Array, typename ...Ts, std::size_t ...I>
auto unrolConvert(const Array &arr, std::index_sequence<I ...>)
{
    return std::make_tuple(fromString<std::remove_cv_t<std::remove_reference_t<Ts>>> (arr[I]) ...);
}

template<typename ...Ts, std::size_t ...I>
inline auto hasValuesImpl(const std::tuple<Ts...> &tup,  std::index_sequence<I ...>) noexcept
{
    return (std::get<I>(tup).has_value() &&...);
}

template<typename ...Ts>
bool hasValues(const std::tuple<Ts...> &tup) noexcept
{
    return hasValuesImpl(tup, std::make_index_sequence<sizeof...(Ts)>());
}
}

template <typename Class, typename ...Ts>
size_t countArgs(void (Class:: *)(Ts...))
{
    return sizeof...(Ts);
}

template <typename Class, typename ...Ts>
bool invokeWithStrings(Class *obj, void (Class:: *fnPtr)(Ts...), const std::vector<std::string> &args)
{
    const auto convertedArgs = Detail::unrolConvert<decltype(args), Ts...>(args, std::make_index_sequence<sizeof...(Ts)>());

    if (!Detail::hasValues(convertedArgs))
    {
        return false;
    }

    std::apply([obj, fnPtr](const auto &...xs)
    {
        (obj->*fnPtr)(xs.value() ...);
    }, convertedArgs);

    return true;
}

}
