/// this header is needed to declare dumbos
#include "dumbocpp.h"

/// fmt for convinient string formatting. library is built with the project.
#include "fmt/format.h"

/// definition of a void function
DUMBO(dumboExampleVoid1, (void))
{
    fmt::print("Hello world from dumbo!\n");
}

/// other way to define a void function
DUMBO_VOID(dumboExampleVoid2)
{
    fmt::print("Hello world from dumbo!\n");
}

/// function that takes a long and a float
DUMBO(dumboExampleAdd, (long i, float f))
{
    fmt::print("{} + {} == {}\n", i, f, (i + f));
}

/// you can pass a std::string
DUMBO(dumboExampleStringArg, (std::string str))
{
    fmt::print("Hello world from dumbo! str: '{}'\n", str);
}

// class with overloaded extraction operator, class needs to be default constructible
class Date
{
public:
    friend std::istream &operator>>(std::istream &is, Date &date)
    {
        is >> date.day >> date.month >> date.year;
        return is;
    }
    int day, month, year;
};

/// you can pass a custom type as long as it has an overload for the extraction operator
/// this function is invoked like this: dumboExampleCustomType "13 10 1989"
DUMBO(dumboExampleCustomType, (const Date &date))
{
    fmt::print("Hello world from dumbo! date: '{}/{}/{}'\n", date.day, date.month, date.year);
}
