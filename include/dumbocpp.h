#pragma once

#include <string>

#include "dumbocppmeta.hpp"


namespace Dumbo
{

/// starts the Dumbo command line interface
bool run();


struct DumboFnBase
{   
    DumboFnBase(const std::string &name, size_t argCount, const std::string &signature, const char *file, int line);
    virtual ~DumboFnBase() = default;

    virtual bool invokeWithStrings(const std::vector<std::string> &args) = 0;

    // wrap members in a struct to not pollute dumbo function scope too much
    struct DumboFnData
    {
        const std::string name;
        const size_t argCount;
        const std::string signature;
        const char *const file;
        const int line;
    };
    DumboFnData dumboFnData;
};

}


#define DUMBO(dumboName, ...)                                                                                          \
    static_assert(sizeof((#dumboName)) > 1, "dumbo name must not be empty");                                           \
    static_assert(sizeof((#__VA_ARGS__)) > 1, "dumbo signature must not be empty. example: '(int i)'");                \
    namespace DumboFnNameSpace                                                                                         \
    {                                                                                                                  \
    struct DumboClass_##dumboName : public Dumbo::DumboFnBase                                                          \
    {                                                                                                                  \
        DumboClass_##dumboName()                                                                                       \
            : Dumbo::DumboFnBase(#dumboName, DumboMeta::countArgs(&DumboClass_##dumboName::fn),                        \
                                 #__VA_ARGS__, __FILE__, __LINE__)                                                     \
        {}                                                                                                             \
                                                                                                                       \
        void fn __VA_ARGS__;                                                                                           \
                                                                                                                       \
        bool invokeWithStrings(const std::vector<std::string> &args) override                                          \
        {                                                                                                              \
            return DumboMeta::invokeWithStrings(this, &DumboClass_##dumboName::fn, args);                              \
        }                                                                                                              \
    };                                                                                                                 \
    DumboClass_##dumboName DumboClassInstance_##dumboName;                                                             \
    }                                                                                                                  \
    void DumboFnNameSpace::DumboClass_##dumboName::fn __VA_ARGS__

#define DUMBO_VOID(dumboName) DUMBO(dumboName, (void))
