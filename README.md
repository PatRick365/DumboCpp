# DumboCpp

A small project that lets you define C++ functions and call them via a command line interface.  

I have written DumboCpp to have a single project where I put all my little test programs for easy access.

![alt text](img/img1.png)

Define a native C++ function using the DUMBO macro:

``` cpp
DUMBO(myFirstDumbo, (const std::string &name, int favoriteNumber))
{
    std::cout << "hey " << name << " i like " << favoriteNumber << " as well!\n";
}
```

Command line interface with tab completion and history provided with the help of [replxx](https://github.com/AmokHuginnsson/replxx):

![alt text](img/img2.png)

Arguments are separated by spaces:

![alt text](img/img3.png)

Arguments are checked for correct type and count:

![alt text](img/img4.png)

Custom types can be used as arguments if they are default constructible and provide an overload for the extraction operator >>.

``` cpp
class Date
{
public:
    friend std::istream &operator>>(std::istream &is, Date &date)
    {
        is >> date.day >> date.month >> date.year;
        return is;
    }
    int day, month, year;
};

DUMBO(dumboExampleCustomType, (const Date &date))
{
    // use date
}
```

To invoke this function: **dumboExampleCustomType "13 10 1989"**

## Using DumboCpp


- Clone the repository **including submodules**  
git clone --recurse-submodules https://gitlab.com/PatRick365/DumboCpp.git
- Create a cpp file in the folder dumbos
- Add the file to the CMakeLists.txt

```
# put your dumbo source files in here
set(DUMBOS
   dumbos/dumbo_examples.cpp
   dumbos/my_dumbos.cpp
)
```

- Include the header **dumbocpp.h** in your file.
- Declare a function like in the examples above.


To declare a void function use either **DUMBO(dumboName, (void)) {}** or **DUMBO_VOID(dumboName) {}**

You can remove or comment out the file dumbo_examples.cpp from CMakeLists.txt to remove the example functions.


## Building the project

```
mkdir build  # Create a directory to hold the build output.
cd build
cmake ..     # Generate native build scripts.
```


## Cross platform

I have only tested DumboCpp on Linux but it should work on Windows and macOS as well.


## Controls

```
Exit DumboCpp: Ctrl+D
Navigate history: Arrow UP/Down
Navigate completions: Ctrl+Arrow UP/Down
```

There are many more keyboard bindings which can be found in the source code, the file is: src/dumbocpp.cpp


## Dependencies

DumboCpp depends on the following libraries:  
[replxx](https://github.com/AmokHuginnsson/replxx)  
[{fmt}](https://github.com/fmtlib/fmt)  

They are included as git submodules, built and statically linked with the project.  
Many thanks to the authors.


## License

DumboCpp is distributed under the MIT license.
